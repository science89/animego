import { AppSettings } from './../../settings/app.settings';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { createSecureServer } from 'http2';
import { User } from 'src/app/modeles/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  findAllUsers(){
    return this.http.get(AppSettings.APP_URL +"/"); 
  }

  findUserById(idUser:number){
    return this.http.get(AppSettings.APP_URL +"/" +idUser);

  }

  saveUser(user: User){
    return this.http.post(AppSettings.APP_URL +"createUser", +user);
  }

  login(mail: string, password:string){
    let param = new HttpParams();
    param.append("mail", mail);
    param.append("password", password);
    return this.http.post(AppSettings.APP_URL +"login",param);
  }
}
