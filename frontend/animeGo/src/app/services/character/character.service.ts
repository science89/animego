import { Character } from './../../modeles/character';
import { AppSettings } from './../../settings/app.settings';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) { }

  findAllCharacter(){
    return this.http.get(AppSettings.APP_URL +"/character/");
  }

  findCharacterById(idCharacter: number){
    return this.http.get(AppSettings.APP_URL +"/character/" +idCharacter);
  }

  saveCharacter(Character: Character){
    return this.http.post(AppSettings.APP_URL+ "/character/creatCharacter/", Character);
  }

  deleteCharacter(idCharacter: number){
    return this.http.delete(AppSettings.APP_URL +'/character/delete/' +idCharacter);
  }

  shareCharacter(idCharacter: number, isShared: boolean){
    return this.http.get(AppSettings.APP_URL +"/character/share/" +idCharacter+ "/" +isShared);
  }

}
