export class Character{
    idCharacter: number;
    characterName: string;
    category: string;
    strength: string;
    shared: boolean;
}