package com.udemy.angular.contollers;

import com.udemy.angular.entities.User;
import com.udemy.angular.repositories.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    private IUser userRepository;

    // Renvoie la Liste des Utilisateurs
    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(userRepository.findAll());
    }

    // Renvoie un utilisateur par son Id
    @GetMapping("/{idUser}")
    public ResponseEntity findUserById(@PathVariable(name = "idUser") Long idUser){
        if (idUser == null){
            return ResponseEntity.badRequest().body("ID null");
        }
        User user = userRepository.getOne((idUser));
        if (user == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(user);

    }

    //Permet de creer un utilisateur
    @PostMapping("/createUser")
    public ResponseEntity createUser(@RequestBody User user){
        if (user == null){
            return ResponseEntity.badRequest().body("Cannot create user with empty fieds");
        }
        User createUser = userRepository.save(user);
        return ResponseEntity.ok(createUser);
    }

    //Authentification
    @PostMapping("/login")
    public ResponseEntity login(@RequestParam(name = "mail") String mail, @RequestParam(name = "password") String password){
        if (StringUtils.isEmpty(mail) || StringUtils.isEmpty(password)) {
            return ResponseEntity.badRequest().body("Cannot login with empty user mail or password");
        }
        User authentificatedUser = userRepository.findByMailAndPassword(mail,password);
        if (authentificatedUser == null){
          return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(authentificatedUser);
    }

}
