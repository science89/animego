package com.udemy.angular.contollers;

import com.udemy.angular.entities.AnimeCharacter;
import com.udemy.angular.repositories.IAnimeCharacter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/character")
public class AnimeController {

    @Autowired
    private IAnimeCharacter characterRepository;

    //Retourne la liste de tous les animes
    @GetMapping("/")
    private ResponseEntity findAll(){return ResponseEntity.ok(characterRepository.findAll());}

    //Retourne l'anime par ID
    @GetMapping("/{idAnime}")
    private ResponseEntity findAnimeById(@PathVariable (name = "idAnime") Long idCharacter){
        if (idCharacter == null){
            return ResponseEntity.badRequest().body("Cannot find anime with null ID");
        }
        AnimeCharacter character = characterRepository.getOne(idCharacter);
        if (character == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(character);
    }

    // Creer un anime
    @PostMapping("/creatCharacter")
    private ResponseEntity createAnime(@RequestBody AnimeCharacter character){
        if (character == null){
            return ResponseEntity.badRequest().body("Cannot create character with empty fieds");
        }
        AnimeCharacter createCharacter = characterRepository.save(character);
        return ResponseEntity.ok(createCharacter);
    }

    // Supprimer un Character
    @DeleteMapping("/delete/{idAnime}")
    public ResponseEntity deleteCharacter(@PathVariable (name = "idAnime") Long idCharacter){
        if (idCharacter == null){
            return ResponseEntity.badRequest().body("Cannot remove character with null ID");
        }
        AnimeCharacter character = characterRepository.getOne(idCharacter);
        if (character == null){
            return ResponseEntity.notFound().build();
        }
        characterRepository.delete(character);
        return ResponseEntity.ok("Character removed with succes!");
    }

    //Partager un character avec d'autres utilisateur
    @GetMapping("/share/{idAnime}/{isShared}")
    private ResponseEntity shareCharacter(@PathVariable (name = "idAnime") Long idAnime, @PathVariable(name = "isShared") boolean isShared){
        if (idAnime == null){
            return ResponseEntity.badRequest().body("Cannot shared character with null ID");
        }
        AnimeCharacter character = characterRepository.getOne(idAnime);
        if (character == null){
            return ResponseEntity.notFound().build();
        }
        character.setShared(isShared);
        return ResponseEntity.ok(characterRepository.save(character));

    }

}
