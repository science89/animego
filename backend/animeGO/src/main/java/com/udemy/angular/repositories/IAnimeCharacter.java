package com.udemy.angular.repositories;

import com.udemy.angular.entities.AnimeCharacter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAnimeCharacter extends JpaRepository<AnimeCharacter, Long> {
}
